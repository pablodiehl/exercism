-- The mentor challenged me to solve the problem inpunting each line of the song only once, here's the result
local house = {}

local song_start = 'This is'
local song_lines = {
  [[ the house that Jack built.]],
  [[ the malt
that lay in]],
  [[ the rat
that ate]],
  [[ the cat
that killed]],
  [[ the dog
that worried]],
  [[ the cow with the crumpled horn
that tossed]],
  [[ the maiden all forlorn
that milked]],
  [[ the man all tattered and torn
that kissed]],
  [[ the priest all shaven and shorn
that married]],
  [[ the rooster that crowed in the morn
that woke]],
  [[ the farmer sowing his corn
that kept]],
  [[ the horse and the hound and the horn
that belonged to]]
}

-- This function builds a song verse by the given verse index
house.verse = function(verse_to_retrieve)
  local verse_build = ''
  for i = 1, verse_to_retrieve do
    verse_build = song_lines[i] .. verse_build
  end

  return song_start .. verse_build
end

-- This function builds the entire song
function house.recite()
  local song_build = ''
  local song_length = #song_lines
  for i = 1, song_length do
    song_build = song_build .. house.verse(i) .. '\n'
  end

  return song_build:sub(1, #song_build - 1)
end


return house