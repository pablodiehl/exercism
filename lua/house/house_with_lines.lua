local house = {}

local song_lines = {
  "This is the house that Jack built.",
  "This is the malt",
  "that lay in the house that Jack built.",
  "This is the rat",
  "that ate the malt",
  "that lay in the house that Jack built.",
  "This is the cat",
  "that killed the rat",
  "that ate the malt",
  "that lay in the house that Jack built.",
  "This is the dog",
  "that worried the cat",
  "that killed the rat",
  "that ate the malt",
  "that lay in the house that Jack built.",
  "This is the cow with the crumpled horn",
  "that tossed the dog",
  "that worried the cat",
  "that killed the rat",
  "that ate the malt",
  "that lay in the house that Jack built.",
  "This is the maiden all forlorn",
  "that milked the cow with the crumpled horn",
  "that tossed the dog",
  "that worried the cat",
  "that killed the rat",
  "that ate the malt",
  "that lay in the house that Jack built.",
  "This is the man all tattered and torn",
  "that kissed the maiden all forlorn",
  "that milked the cow with the crumpled horn",
  "that tossed the dog",
  "that worried the cat",
  "that killed the rat",
  "that ate the malt",
  "that lay in the house that Jack built.",
  "This is the priest all shaven and shorn",
  "that married the man all tattered and torn",
  "that kissed the maiden all forlorn",
  "that milked the cow with the crumpled horn",
  "that tossed the dog",
  "that worried the cat",
  "that killed the rat",
  "that ate the malt",
  "that lay in the house that Jack built.",
  "This is the rooster that crowed in the morn",
  "that woke the priest all shaven and shorn",
  "that married the man all tattered and torn",
  "that kissed the maiden all forlorn",
  "that milked the cow with the crumpled horn",
  "that tossed the dog",
  "that worried the cat",
  "that killed the rat",
  "that ate the malt",
  "that lay in the house that Jack built.",
  "This is the farmer sowing his corn",
  "that kept the rooster that crowed in the morn",
  "that woke the priest all shaven and shorn",
  "that married the man all tattered and torn",
  "that kissed the maiden all forlorn",
  "that milked the cow with the crumpled horn",
  "that tossed the dog",
  "that worried the cat",
  "that killed the rat",
  "that ate the malt",
  "that lay in the house that Jack built.",
  "This is the horse and the hound and the horn",
  "that belonged to the farmer sowing his corn",
  "that kept the rooster that crowed in the morn",
  "that woke the priest all shaven and shorn",
  "that married the man all tattered and torn",
  "that kissed the maiden all forlorn",
  "that milked the cow with the crumpled horn",
  "that tossed the dog",
  "that worried the cat",
  "that killed the rat",
  "that ate the malt",
  "that lay in the house that Jack built."
}

-- This function returns a string containg all the song lines form a given first line to a given last line
local function build_string(first_line, last_line)
  local song = ""
  for i = first_line, last_line do
    song = song .. song_lines[i] .. "\n"
  end
  return song:sub(1, #song - 1) -- Let's remove the last "\n" from the song
end

-- This function retrieves a verse of the song
-- * verse_to_be_retrieved: is expected to be an integer
function house.verse(verse_to_be_retrieved)
  -- Let's search throughout the whole song to find out the lines where each new verse starts
  -- (a new verse is every new line which starts with uppercase)
  local song_length = #song_lines
  local lines_which_start_new_verses = {}
  for i = 1, song_length do
    local first_char_of_line = song_lines[i]:sub(1, 1)
    if first_char_of_line == first_char_of_line:upper() then
      table.insert(lines_which_start_new_verses, i)
    end
  end

  -- Now let's start building the verse
  local line_to_start_verse = lines_which_start_new_verses[verse_to_be_retrieved]  -- Set the line to start
  local line_to_finish_the_verse
  -- The end of the verse will be the previous line before the line which starts the next verse,
  -- unless if this is the last verse of the song, so the last line will be... the last line
  if verse_to_be_retrieved == #lines_which_start_new_verses then
    line_to_finish_the_verse = #song_lines
  else
    line_to_finish_the_verse = lines_which_start_new_verses[verse_to_be_retrieved + 1] - 1
  end

  return build_string(line_to_start_verse, line_to_finish_the_verse)
end

-- This functions returns a single String containing the entire song
function house.recite()
  local song_length = #song_lines
  return build_string(1, song_length)
end

return house