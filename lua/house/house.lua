local house = {}

local verses = {
  [[This is the house that Jack built.]],
  [[This is the malt
that lay in the house that Jack built.]],
  [[This is the rat
that ate the malt
that lay in the house that Jack built.]],
  [[This is the cat
that killed the rat
that ate the malt
that lay in the house that Jack built.]],
  [[This is the dog
that worried the cat
that killed the rat
that ate the malt
that lay in the house that Jack built.]],
  [[This is the cow with the crumpled horn
that tossed the dog
that worried the cat
that killed the rat
that ate the malt
that lay in the house that Jack built.]],
  [[This is the maiden all forlorn
that milked the cow with the crumpled horn
that tossed the dog
that worried the cat
that killed the rat
that ate the malt
that lay in the house that Jack built.]],
  [[This is the man all tattered and torn
that kissed the maiden all forlorn
that milked the cow with the crumpled horn
that tossed the dog
that worried the cat
that killed the rat
that ate the malt
that lay in the house that Jack built.]],
  [[This is the priest all shaven and shorn
that married the man all tattered and torn
that kissed the maiden all forlorn
that milked the cow with the crumpled horn
that tossed the dog
that worried the cat
that killed the rat
that ate the malt
that lay in the house that Jack built.]],
  [[This is the rooster that crowed in the morn
that woke the priest all shaven and shorn
that married the man all tattered and torn
that kissed the maiden all forlorn
that milked the cow with the crumpled horn
that tossed the dog
that worried the cat
that killed the rat
that ate the malt
that lay in the house that Jack built.]],
  [[This is the farmer sowing his corn
that kept the rooster that crowed in the morn
that woke the priest all shaven and shorn
that married the man all tattered and torn
that kissed the maiden all forlorn
that milked the cow with the crumpled horn
that tossed the dog
that worried the cat
that killed the rat
that ate the malt
that lay in the house that Jack built.]],
  [[This is the horse and the hound and the horn
that belonged to the farmer sowing his corn
that kept the rooster that crowed in the morn
that woke the priest all shaven and shorn
that married the man all tattered and torn
that kissed the maiden all forlorn
that milked the cow with the crumpled horn
that tossed the dog
that worried the cat
that killed the rat
that ate the malt
that lay in the house that Jack built.]]
}

-- This function retrieves a verse of the song
-- * verse_to_be_retrieved: is expected to be an integer
function house.verse(verse_to_be_retrieved)
  return verses[verse_to_be_retrieved]
end

--[[
  -- This functions returns a single String containing the entire song
  function house.recite()
    return table.concat(verses, '\n')
  end
]]

-- While the code above expresses the (I guess) most simple way to solve this problem using Lua
-- I decide to write a function without using built-in resources from the language, this way the
-- solution wouldn't be just taking a look at the test file and copying the solution from there.

-- This functions returns a single String containing the entire song
function house.recite()
  local song = '' -- Let's initiate a string to concat the whole song
  local song_length = #verses
  for i = 1, song_length do
    song = song .. verses[i] .. '\n'
  end
  return song:sub(1, #song - 1) -- Let's remove the last '\n' from the song
end

return house