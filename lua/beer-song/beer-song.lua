local beer = {}

local function get_second_verse(number)
    if number > 1 then
        return string.format(
            "Take one down and pass it around, %s bottles of beer on the wall.\n",
            number
        )
    end

    return "Take one down and pass it around, 1 bottle of beer on the wall.\n"
end

function beer.verse(number)
    if number > 1 then
        return string.format(
            '%s bottles of beer on the wall, %s bottles of beer.\n%s',
            number,
            number,
            get_second_verse(number - 1)
        )
    elseif number == 1 then
        return '1 bottle of beer on the wall, 1 bottle of beer.\nTake it down and pass it around, no more bottles of beer on the wall.\n'
    else
        return 'No more bottles of beer on the wall, no more bottles of beer.\nGo to the store and buy some more, 99 bottles of beer on the wall.\n'
    end
end

function beer.sing(start, finish)
    finish = finish or 0
    local song = {}
    for i = start, finish, -1 do
        table.insert(song, beer.verse(i))
    end

    return table.concat(song, '\n')
end

return beer
