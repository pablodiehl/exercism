-- Converts a given DNA strand into a RNA strand
local function to_rna(strand)
  -- Substitute any word character in the DNA strand for its RNA equivalent (if any)
  return strand:gsub("%w", {C="G", G="C", T="A", A="U"})
end

return to_rna