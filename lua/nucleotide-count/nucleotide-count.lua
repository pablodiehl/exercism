local Dna = { sequence = '' }

function Dna:new(sequence)
  self.__index = self
  self.sequence = sequence or ''
  self.nucleotideCounts = { A = 0, T = 0, G = 0, C = 0 }
  local sequence_length = #sequence
  for i = 1, sequence_length do
    local char = sequence:sub(i, i)
    self.nucleotideCounts[char] = self.nucleotideCounts[char] + 1
  end
  return self
end

-- Returns the amount of a given nucleotide present in the DNA
function Dna:count(nucleotide)
  if self.nucleotideCounts[nucleotide] then
    return self.nucleotideCounts[nucleotide]
  end
  return error('Invalid Nucleotide')
end

return Dna
