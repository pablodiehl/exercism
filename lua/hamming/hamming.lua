local hamming = {}

-- This function returns the 'Hamming distance' between two DNA strands.
function hamming.compute(first_dna_strand, second_dna_strand)
  --[[
    The first thing we must check is the size of the strands, if they have different
    sizes, we don't even need to check the difference between their elements.
  ]]
  if #first_dna_strand ~= #second_dna_strand then
    return -1
  end

  -- If the strands have the same size, then now we have to check their elements
  local strands_length = #first_dna_strand
  local differences = 0
  for i = 1, strands_length do
    local first_dna_strand_char = first_dna_strand:byte(i)
    local second_dna_strand_char = second_dna_strand:byte(i)

    if first_dna_strand_char ~= second_dna_strand_char then
      differences = differences + 1
    end
  end

  return differences
end

return hamming