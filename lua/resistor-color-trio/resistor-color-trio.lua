local colors = {
    black = 0,
    brown = 1,
    red = 2,
    orange = 3,
    yellow = 4,
    green = 5,
    blue = 6,
    violet = 7,
    grey = 8,
    white = 9
}

local function get_unit(param)
    if param < 1000 then
        return param, 'ohms'
    elseif param < 1000000 then
        return param/1000, 'kiloohms'
    elseif param < 1000000000 then
        return param/1000000, 'megaohms'
    else
        return param/1000000000, 'gigaohms'
    end
end

return {
    decode = function(c1, c2, c3)
        local value = tonumber(colors[c1] .. colors[c2] .. string.rep("0", colors[c3]))

        return get_unit(value)
    end
}
