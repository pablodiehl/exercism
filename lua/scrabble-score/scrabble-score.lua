local _M = {}

local score_table = {
    A = 1, E = 1, I = 1, O = 1, U = 1, L = 1, N = 1, R = 1, S = 1, T = 1,
    D = 2, G = 2,
    B = 3, C = 3, M = 3, P = 3,
    F = 4, H = 4, V = 4, W = 4, Y = 4,
    K = 5,
    J = 8, X = 8,
    Q = 10, Z = 10
}

--[[
This functions returns the score of a given string

@param <string> input: string to be calculated

@return <number> string_score: the score of the given string
]]
function _M.score(input)
    local string_score = 0

    -- If the input is not a string we don't need to calculate the score
    if type(input) ~= "string" then return string_score end

    for i = 1, #input do
        local c = string.upper(input:sub(i,i))

        if score_table[c] then
            string_score = string_score + score_table[c]
        end
    end

    return string_score
end


return _M