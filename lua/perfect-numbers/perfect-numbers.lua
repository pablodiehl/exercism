local function aliquot_sum(n)
    local sum = 0
    local limit = math.floor(n/2)  -- Half of the input is the limit we should use
    for i=1, limit do
        if (n % i) == 0 then
            sum = sum + i
        end
    end

    return sum
end

local function classify(n)
    local sum = aliquot_sum(n)

    if sum > n then
        return 'abundant'
    elseif sum < n then
        return 'deficient'
    else
        return 'perfect'
    end
end

return {
  aliquot_sum = aliquot_sum,
  classify = classify
}
