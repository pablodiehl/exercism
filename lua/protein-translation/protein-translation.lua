local translations = {
  ['AUG'] = 'Methionine',
  ['UUU'] = 'Phenylalanine',
  ['UUC'] = 'Phenylalanine',
  ['UUA'] = 'Leucine',
  ['UUG'] = 'Leucine' ,
  ['UCU'] = 'Serine',
  ['UCC'] = 'Serine',
  ['UCA'] = 'Serine',
  ['UCG'] = 'Serine',
  ['UAU'] = 'Tyrosine',
  ['UAC'] = 'Tyrosine',
  ['UGU'] = 'Cysteine',
  ['UGC'] = 'Cysteine',
  ['UGG'] = 'Tryptophan',
  ['UAA'] = 'STOP',
  ['UAG'] = 'STOP',
  ['UGA'] = 'STOP',
}

-- Returns a protein (if any) by a given codon sequence
local function codon(sequence)
  local protein = translations[sequence]

  if protein then
    return protein
  end

  -- If no protein is found, raises an erron
  error()
end

-- Returns the proteins of a given rna strand
local function rna_strand(rna)
  local proteins = {}
  local rna_length = #rna

  -- Using a three positions increment step here because each codon is composed by three letters
  for i = 1, rna_length, 3 do
    local protein = codon(rna:sub(i, i+2))

    if protein == 'STOP' then
      return proteins
    end

    proteins[#proteins + 1] = protein
  end

  return proteins
end


return { codon = codon, rna_strand = rna_strand}